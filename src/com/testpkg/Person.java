package com.testpkg;

import java.util.ArrayList;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

public class Person extends GenericService {
	private String name;
	private int age;
	private Disposition moodiness;
	private MutableRepository personRepo;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Disposition getMoodiness() {
		return moodiness;
	}

	public void setMoodiness(Disposition moodiness) {
		this.moodiness = moodiness;
		System.out.println("Making " + this.name + " feel " + this.moodiness.getMood());
	}

	public MutableRepository getPersonRepo() {
		return personRepo;
	}

	public void setPersonRepo(MutableRepository personRepo) {
		this.personRepo = personRepo;
	}

	public void createRepoItem(String name, Integer age) {
		try {
			MutableRepositoryItem userItem = getPersonRepo().createItem("dude");
			userItem.setPropertyValue("name", name);
			userItem.setPropertyValue("age", age.toString());
			userItem.setPropertyValue("userId", userItem.getRepositoryId());
			userItem.setPropertyValue("addressId", null);
			getPersonRepo().addItem(userItem);
		} catch (RepositoryException rpe) {
			if (isLoggingError()) {
				logError("Could not create repository item");
			}
		}
	}
	
	public ArrayList<String> getRepoItemId(String name) {
		ArrayList<String> itemIds = new ArrayList<String>();
		try {
			RepositoryView view = getPersonRepo().getView("dude");
			QueryBuilder qBuilder = view.getQueryBuilder();
			QueryExpression nameQ1 = qBuilder.createConstantQueryExpression(name);
			QueryExpression nameQ2 = qBuilder.createPropertyQueryExpression("name");
			Query query = qBuilder.createIncludesAnyQuery(nameQ1, nameQ2);
			RepositoryItem[] items = view.executeQuery(query);
			for (RepositoryItem item : items) {
				itemIds.add(item.getRepositoryId());
			}
			return itemIds;
		} catch (RepositoryException rpe) {
			if (isLoggingError()) {
				logError("Could not get repo item");
			}
			return null;
		}
	}
	
	/*
	 * Constructors
	 */
	public Person() {
	}
	
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
		System.out.println("You made a Person named " + this.name + " who is " + this.age + " years old!");
	}
	
	@Override
	public void doStartService() throws ServiceException {
		super.doStartService();
		System.out.println("Service started!");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Person bob = new Person("J.R. \"Bob\" Dobbs", 37);
		System.out.println(bob.getName() + " is feeling " + bob.moodiness.getMood());
	}
}
