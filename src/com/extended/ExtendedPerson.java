package com.extended;

import com.testpkg.Person;

public class ExtendedPerson extends Person {
	String gender;
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public ExtendedPerson() {
		super();
	}

}
