package com.testdrops;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;

import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class HelloDroplet extends DynamoServlet {
	private Repository helloRepo;
	
	public Repository getHelloRepo() {
		return helloRepo;
	}

	public void setHelloRepo(Repository helloRepo) {
		this.helloRepo = helloRepo;
	}

	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {
		final String OPARAM_FAIL = "failure";
		final String OPARAM_SUCCESS = "success";
		
		Repository repo = this.getHelloRepo();
		ArrayList<String> nameList = new ArrayList<String>();
		
		res.getOutputStream().println("Hallo Welt!");
		
		try {
			RepositoryView view = repo.getView("dude");
			QueryBuilder qBuilder = view.getQueryBuilder();
			//QueryExpression qExp1 = qBuilder.createPropertyQueryExpression("age");
			//QueryExpression qExp2 = qBuilder.createConstantQueryExpression(new Integer(0));
			QueryExpression qExp1 = qBuilder.createPropertyQueryExpression("name");
			QueryExpression qExp2 = qBuilder.createConstantQueryExpression(" ");
			Query query = qBuilder.createComparisonQuery(qExp1, qExp2, QueryBuilder.NOT_EQUALS);
			RepositoryItem[] items = view.executeQuery(query);
			for (RepositoryItem item : items) {
				nameList.add(item.getPropertyValue("name").toString());
			}
			req.setParameter("nameList", nameList);
			req.serviceParameter(OPARAM_SUCCESS, req, res);
		} catch (RepositoryException rpe) {
			rpe.printStackTrace();
			// set failure param for JSP
			req.serviceParameter(OPARAM_FAIL, req, res);
		}
	}
}
