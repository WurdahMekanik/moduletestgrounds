package com.testdrops;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

public class FormDropHandler extends GenericFormHandler {
	private String name;
	private int age;
	//private String email;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	/*public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}*/
	
	/*boolean noValue(String s) {
		boolean vResult = ((s == null)|| (s.length() == 0));
		return vResult;
	}*/
	
	public boolean handleSubmit (DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// validate data
		logDebug("Inside formhandler");
		return true;
		// submit data
		
		// post-submission process
	}
	
}
