<!--%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" -->

<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<dsp:importbean bean="/atg/testdrops/HelloDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hello Test</title>
</head>
<body>
	Droplet Text: <br>
	<dsp:droplet name="/atg/testdrops/HelloDroplet">	
  		<!-- display "no." upon failure -->
  		<dsp:oparam name="failure">
  			no.
  		</dsp:oparam>
  		<!-- use ForEach droplet to display array values upon success -->
	  	<dsp:oparam name="success">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="nameList"/>
				<dsp:oparam name="empty">
					Empty!
				</dsp:oparam>
				<dsp:oparam name="outputStart">
					<p>Listing elements in repo:</p>
  				</dsp:oparam>
  				<ul>
	  			<dsp:oparam name="output">
	    			<li><dsp:valueof param="element"/></li>
  				</dsp:oparam>
  				</ul>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</body>
</dsp:page>