<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>

<dsp:page>
<dsp:importbean bean="/atg/testdrops/FormDropHandler"/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
</head>
<body>
<dsp:form action="hello.jsp" method="POST">
	Enter your name:
	<dsp:input name="userName" bean="FormDropHandler.name" type="text"/>
	Enter your age:
	<dsp:input name="userAge" bean="FormDropHandler.age" type="text"/>
	<%--Enter your email address:
	<dsp:input name="userEmail" bean="FormDropHandler.email" type="text"/>
	--%>
	
	<dsp:input type="submit" bean="FormDropHandler.submit" value="Submit"/>
</dsp:form>
</body>
</dsp:page>